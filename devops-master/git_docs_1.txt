1. git branch <new_branch_name>

2. git checkout <new_branch_name>

3. make the changes and save.

4. git status

5. git add -A

6. git status

7. git commit -m "commit_message"

8. git push -u origin <new_branch_name>

9.git checkout master

10. git pull origin master

11. git merge <new_branch_name> : git merge --squash branch_name whose final changes we want in master branch

12. git push origin master

13. git branch --merged

14. git branch -d <new_branch_name> (to delete new branch locally)

15. git branch -a

16. git push origin --delete <new_branch_name> (to delete new branch remotely)

17. git branch -a

18. git diff file_name (for differences with index)

19. git diff --cached file_name (for changes that are commited)

20. git diff master origin/master (for changes that are fetched)

21. git merge (To merge the fetched changes, Note: close the file which is supposed to be changed before merge command)

22. git rebase master (we should be in other branch than master to rebase the other branch)

23. git log -n 5 (To see last 5 commits)

24. git log --oneline (logs with short discriptions)

25. git checkout file_name (To discard the changes made to the file)

26. git checkout . (To discard all the changes in working area)

27. git reset HEAD file_name (To unstage the changes staged to the file)

28. git reset HEAD . (To unstage all the changes in working area)

28. git reset HEAD~3 (To remove recent 3 commits from history)

29. git checkout -- . (To go back to previous commit)

30. git stash save "message" (To save changes temorarily, not in working directory, instead on some temporary location)

31. git stash list (To see the list of stashed changes)

32. git stash apply stash_id (To apply the stashed change, but it won't delete stashed changes from stash list)

33. git stash pop (To apply the stashed change and it will also sdelete stashed changes from stash list)

